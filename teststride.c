//Stride Scheduling test program
#include "types.h"
#include "stat.h"
#include "user.h"

#define TIMEWASTER 11234567890
#define PROCESSOS_T 3
void timewaster(void);

int main (void){
  int i, n;
  printf(1, "\n\nPrograma Teste\n");
  printf(1, "\n CTRL + P para visualizar os processos\n");
  for(i = 0, n = 50; i < PROCESSOS_T; i++, n += 50){
    printf(1, "Process %d is having a baby with %d tickets!\n", getpid(), n);
    if(fork(n) == 0){
      timewaster();
      printf(1, "Process %d is over. nber of Tickets: %d\n", getpid(), n);
      exit();
    }
  }
  for(i = 0; i < PROCESSOS_T; i++) wait(); //Wait for Children to exit
  exit();
}
void timewaster(void){
  int j;
  for(j = 0; j < TIMEWASTER; j++);
}
